# SPDX-FileCopyrightText: 2020 Linus Jahn <lnj@kaidan.im>
# SPDX-FileCopyrightText: 2020 Jonah Brüchert <jbb@kaidan.im>
#
# SPDX-License-Identifier: CC0-1.0

cmake_minimum_required(VERSION 3.5)

project(kjson2cpp LANGUAGES CXX)

option(BUILD_TESTS "Build and run unit tests" ON)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

include(FeatureSummary)

find_package(ECM REQUIRED)

# where to look first for cmake modules, before ${CMAKE_ROOT}/Modules/ is checked
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${ECM_MODULE_PATH} ${ECM_KDE_MODULE_DIR})

include(KDEInstallDirs)

# dependencies
find_package(Qt5 5.14.0 REQUIRED COMPONENTS Core Xml Test)

if(BUILD_TESTS)
    find_package(ECM REQUIRED)
    set(CMAKE_MODULE_PATH ${ECM_MODULE_PATH})
endif()

# sources
add_subdirectory(src)

# test
if(BUILD_TESTS)
    add_subdirectory(tests)
endif()

feature_summary(WHAT ALL FATAL_ON_MISSING_REQUIRED_PACKAGES)
