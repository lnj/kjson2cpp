// SPDX-FileCopyrightText: 2020 Linus Jahn <lnj@kaidan.im>
// SPDX-FileCopyrightText: 2020 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#include "jsonschema.h"

#include <QDebug>
#include <QDomElement>
#include <QSet>
#include <QStringView>

#include <ranges>

namespace JsonSchema {

constexpr QStringView SCHEMA_TAG = u"json-schema";
constexpr QStringView KEY_CONVERSION_TAG = u"key-conversion";
constexpr QStringView GETTER_STYLE_TAG = u"getter-style";
constexpr QStringView CLASS_TAG = u"class";
constexpr QStringView ATTRIBUTE_TAG = u"attribute";
constexpr std::array<QStringView, 12> JSON_ATTRIBUTE_TYPES = {
    u"object",
    u"array",
    u"string",
    u"bool",
    u"int8",
    u"int16",
    u"int32",
    u"int64",
    u"uint8",
    u"uint16",
    u"uint32",
    u"uint64",
};
constexpr std::array<QStringView, 12> JSON_ATTRIBUTE_CPP_TYPES = {
    QStringView(),
    QStringView(),
    u"QString",
    u"bool",
    u"int8_t",
    u"int16_t",
    u"int32_t",
    u"int64_t",
    u"uint8_t",
    u"uint16_t",
    u"uint32_t",
    u"uint64_t",
};

constexpr std::array<QStringView, 3> JSON_KEY_CASING_TYPES = {
    u"camelCase",
    u"snakeCase",
    u"kebapCase"
};

constexpr std::array<QStringView, 2> GETTER_STYLE_TYPES = {u"include-get", u"without-get"};

/************************************************
 * Attribute
 ************************************************/

std::optional<Attribute::JsonType> Attribute::jsonTypeFromString(QStringView string)
{
    if (const auto itr = std::ranges::find(JSON_ATTRIBUTE_TYPES, string);
        itr != std::end(JSON_ATTRIBUTE_TYPES)) {
        return JsonType(std::distance(std::begin(JSON_ATTRIBUTE_TYPES), itr));
    }
    return std::nullopt;
}

QStringView Attribute::jsonTypeToString(JsonType type)
{
    return JSON_ATTRIBUTE_TYPES[std::size_t(type)];
}

std::optional<Attribute> Attribute::fromDom(const QDomElement &element)
{
    if (element.tagName() != ATTRIBUTE_TAG)
        return std::nullopt;

    Attribute attr;
    if (const auto type = jsonTypeFromString(element.attribute(QStringLiteral("type")))) {
        attr.m_type = *type;
    } else {
        return std::nullopt;
    }
    attr.m_jsonKey = element.attribute(QStringLiteral("json-key"));
    attr.m_typeReference = element.attribute(QStringLiteral("reference"));

    return attr;
}

Attribute::Attribute() : m_type(Object) {}

Attribute::JsonType Attribute::type() const
{
    return m_type;
}

QString Attribute::jsonKey() const
{
    return m_jsonKey;
}

QString Attribute::typeReference() const
{
    return m_typeReference;
}

QString Attribute::cppType() const
{
    switch (m_type) {
    case Object:
        return typeReference();
        break;
    case Array:
        return QStringLiteral("QList<") + m_typeReference + QStringLiteral(">");
        break;
    default:
        return JSON_ATTRIBUTE_CPP_TYPES.at(int(m_type)).toString();
    }
    return {};
}

QString Attribute::cppSetterType() const
{
    switch (m_type) {
    case Bool:
    case Int8:
    case Int16:
    case Int32:
    case Int64:
    case UInt8:
    case UInt16:
    case UInt32:
    case UInt64:
        return JSON_ATTRIBUTE_CPP_TYPES.at(int(m_type)).toString();
    default:
        return QStringView(u"const %1 &").arg(cppType());
    }
}

/************************************************
 * Class
 ************************************************/

std::optional<Class> Class::fromDom(const QDomElement &element)
{
    if (element.tagName() != CLASS_TAG)
        return std::nullopt;

    Class schemaClass;
    schemaClass.m_name = element.attribute(QStringLiteral("name"));

    for (auto child = element.firstChildElement(); !child.isNull();
         child = child.nextSiblingElement()) {
        if (child.tagName() == ATTRIBUTE_TAG) {
            if (const auto attributeSchema = Attribute::fromDom(child)) {
                schemaClass.m_attributes << *attributeSchema;
            }
        }
    }
    return schemaClass;
}

QString Class::name() const
{
    return m_name;
}

void Class::setName(const QString &name)
{
    m_name = name;
}

QList<Attribute> Class::attributes() const
{
    return m_attributes;
}

void Class::setAttributes(const QList<Attribute> &attributes)
{
    m_attributes = attributes;
}

/************************************************
 * Schema
 ************************************************/

std::optional<Schema> Schema::fromDom(const QDomElement &element)
{
    if (element.tagName() != SCHEMA_TAG)
        return std::nullopt;

    Schema schema;

    for (auto child = element.firstChildElement(); !child.isNull();
         child = child.nextSiblingElement()) {
        if (child.tagName() == CLASS_TAG) {
            if (const auto classSchema = Class::fromDom(child)) {
                schema.m_classes << *classSchema;
            }
        } else if (child.tagName() == KEY_CONVERSION_TAG) {
            schema.m_keyCasing = keyCasingFromString(child.attribute(QStringLiteral("type")));
        } else if (child.tagName() == GETTER_STYLE_TAG) {
            schema.m_getterNameStyle = getterNameStyleFromString(
                                           child.attribute(QStringLiteral("type")))
                                           .value_or(WithoutGet);
        }
    }

    return schema;
}

Schema::Schema() : m_getterNameStyle(WithoutGet) {}

std::optional<Schema::KeyCasing> Schema::keyCasing() const
{
    return m_keyCasing;
}

QList<Class> Schema::classes() const
{
    return m_classes;
}

QString Schema::formatKeyName(QStringView name)
{
    if (!m_keyCasing) {
        return name.toString();
    }

    switch (*m_keyCasing) {
    case CamelCase:
    case SnakeCase:
    case KebapCase:
        return name.toString();
    }
    return {};
}

QSet<QString> Schema::referencedClasses(const QString &className) const
{
    QStringList classNames;
    classNames.reserve(m_classes.size());

    for (const auto &classSchema : std::as_const(m_classes)) {
        classNames << classSchema.name();
    }

    const auto itr = std::ranges::find_if(std::as_const(m_classes),
                                          [&](const JsonSchema::Class &classSchema) {
                                              return classSchema.name() == className;
                                          });
    if (itr == m_classes.cend())
        return {};

    QSet<QString> referencedClasses;

    const auto attributes = itr->attributes();
    for (const auto &attribute : attributes) {
        if (!attribute.typeReference().isEmpty() && classNames.contains(attribute.typeReference())) {
            referencedClasses << attribute.typeReference();
        }
    }

    return referencedClasses;
}

QString Schema::getterName(const Attribute &attribute)
{
    switch (m_getterNameStyle) {
    case IncludeGet:
        return prependToFunctionName(formatKeyName(attribute.jsonKey()), u"get");
    case WithoutGet:
        return formatKeyName(attribute.jsonKey());
    }
    return {};
}

QString Schema::setterName(const Attribute &attribute)
{
    return prependToFunctionName(formatKeyName(attribute.jsonKey()), u"set");
}

QString Schema::prependToFunctionName(QStringView string, QStringView prepend)
{
    if (string.isEmpty()) {
        qFatal("Key name MUST have a value");
    }

    // make first char uppercase
    if (!m_keyCasing || (m_keyCasing && m_keyCasing.value() == CamelCase)) {
        return QStringView(u"%1%2%3").arg(prepend, string.at(0).toUpper(), string.mid(1));
    }
    return QStringView(u"%1%2").arg(prepend, string);
}

std::optional<Schema::KeyCasing> Schema::keyCasingFromString(const QString &string)
{
    for (unsigned long i = 0; i < JSON_KEY_CASING_TYPES.size(); i++) {
        if (string == JSON_KEY_CASING_TYPES[i]) {
            return KeyCasing(i);
        }
    }

    return std::nullopt;
}

std::optional<Schema::GetterNameStyle> Schema::getterNameStyleFromString(const QString &string)
{
    if (const auto itr = std::ranges::find(GETTER_STYLE_TYPES, string);
        itr != std::end(GETTER_STYLE_TYPES)) {
        return GetterNameStyle(std::distance(std::begin(GETTER_STYLE_TYPES), itr));
    }
    return std::nullopt;
}

} // namespace JsonSchema
