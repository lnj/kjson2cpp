// SPDX-FileCopyrightText: 2020 Linus Jahn <lnj@kaidan.im>
// SPDX-FileCopyrightText: 2020 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#include "classgenerator.h"

#include <QDomDocument>
#include <QFile>
#include <QSet>
#include <QStringBuilder>

ClassGenerator::ClassGenerator(CommandLineParser &&parser)
    : m_parser(std::move(parser)), m_currentClass(nullptr)
{}

void ClassGenerator::generateCode()
{
    QFile inputFile(m_parser.inputFile());
    if (!inputFile.open(QIODevice::ReadOnly)) {
        qFatal("Could not open file %s for reading", qPrintable(m_parser.inputFile()));
    }

    QDomDocument document;
    document.setContent(&inputFile, false);
    if (auto schema = JsonSchema::Schema::fromDom(document.documentElement())) {
        m_jsonSchema = *schema;
    } else {
        qFatal("Could not parse JSON schema");
    }

    if (!m_parser.outputDir().exists()) {
        if (!m_parser.outputDir().mkpath(QStringLiteral("."))) {
            qFatal("Could not create output directory: %s",
                   qPrintable(m_parser.outputDir().absolutePath()));
        }
    }

    const auto classes = m_jsonSchema.classes();
    for (const auto &jsonClass : classes) {
        m_currentClass = &jsonClass;
        generateFilesForClass();
    }
}

void ClassGenerator::generateFilesForClass()
{
    createHeaderFile();
    createCppFile();

    // generate target.cpp
    QFile targetCppFile(m_parser.outputDir().filePath(QStringLiteral("target.cpp")));
    if (!targetCppFile.open(QIODevice::WriteOnly)) {
        qFatal("Could not open file %s for writing output: %s",
               qPrintable(targetCppFile.fileName()),
               qPrintable(targetCppFile.errorString()));
    }

    const auto classes = m_jsonSchema.classes();
    for (const auto &classSchema : classes) {
        writeInclude(targetCppFile, classSchema.name().append(QLatin1String(".cpp")), false);
    }
}

void ClassGenerator::createHeaderFile()
{
    m_header.setFileName(m_parser.outputDir().filePath(m_currentClass->name() % u".h"));
    if (!m_header.open(QIODevice::WriteOnly)) {
        qFatal("Could not open file %s for writing output: %s",
               qPrintable(m_header.fileName()),
               qPrintable(m_header.errorString()));
    }

    writeClassHeader();

    m_header.close();
}

void ClassGenerator::createCppFile()
{
    m_cpp.setFileName(m_parser.outputDir().filePath(m_currentClass->name() % u".cpp"));
    if (!m_cpp.open(QIODevice::WriteOnly)) {
        qFatal("Could not open file %s for writing output: %s",
               qPrintable(m_cpp.fileName()),
               qPrintable(m_cpp.errorString()));
    }

    writeInclude(m_cpp, m_currentClass->name().append(QLatin1String(".h")), false);
    writeInclude(m_cpp, u"QJsonObject");

    const auto headers = m_jsonSchema.referencedClasses(m_currentClass->name());
    for (const auto &header : headers) {
        writeInclude(m_cpp, header + QStringLiteral(".h"), false);
    }

    m_cpp.write("\n");
    writePrivateClass();
    writeDefinitions();

    m_cpp.close();
}

void ClassGenerator::writeClassHeader()
{
    const auto className = m_currentClass->name().toUtf8();

    writeInclude(m_header, u"QSharedDataPointer");
    m_header.write("\n");
    forwardDeclare(m_header, u"QJsonObject");

    const auto referencedClasses = m_jsonSchema.referencedClasses(m_currentClass->name());
    for (const auto &referencedClass : referencedClasses) {
        forwardDeclare(m_header, referencedClass);
    }

    // forward declare private class
    forwardDeclare(m_header, m_currentClass->name() + QStringLiteral("Private"));

    m_header.write("\n");

    m_header.write("class ");
    m_header.write(className);
    m_header.write("\n{\n"
                   "public:\n");

    // static fromJson() function
    indent(m_header);
    m_header.write(QStringView(u"static std::optional<%1> fromJson(const QJsonObject &);\n\n")
                       .arg(m_currentClass->name())
                       .toUtf8());

    // ctor
    indent(m_header);
    m_header.write(className);
    m_header.write("();\n");
    // copy ctor
    indent(m_header);
    m_header.write(QByteArrayLiteral("%1(const %1 &);\n").replace("%1", className));
    // dtor
    indent(m_header);
    m_header.write("~");
    m_header.write(className);
    m_header.write("();\n");
    // assignment operator
    indent(m_header);
    m_header.write(QByteArrayLiteral("%1 &operator=(const %1 &);\n\n").replace("%1", className));

    const auto attributes = m_currentClass->attributes();
    for (const auto &attribute : attributes) {
        writeGetterSetterHeader(attribute);
    }

    m_header.write("private:\n");
    indent(m_header);
    m_header.write("QSharedDataPointer<");
    m_header.write(className);
    m_header.write("Private> d;\n");
    m_header.write("};\n");
}

void ClassGenerator::writeGetterSetterHeader(const JsonSchema::Attribute &attribute)
{
    // getter
    // <indent>Key key() const;
    indent(m_header);
    m_header.write(attribute.cppType().toUtf8());
    m_header.write(QByteArrayLiteral(" "));
    m_header.write(m_jsonSchema.getterName(attribute).toUtf8());
    m_header.write(QByteArrayLiteral("() const;\n"));

    // setter
    // <indent>void setKey(const Key &);
    indent(m_header);
    m_header.write(QByteArrayLiteral("void "));
    m_header.write(m_jsonSchema.setterName(attribute).toUtf8());
    m_header.write(QByteArrayLiteral("("));
    m_header.write(attribute.cppSetterType().toUtf8());
    m_header.write(QByteArrayLiteral(");\n\n"));
}

void ClassGenerator::writePrivateClass()
{
    const QByteArray className = m_currentClass->name().toUtf8();

    m_cpp.write("class ");
    m_cpp.write(className);
    m_cpp.write("Private : public QSharedData\n{\n");
    m_cpp.write("public:\n");

    const auto attributes = m_currentClass->attributes();
    for (const auto &attribute : attributes) {
        writeAttribute(attribute);
    }

    m_cpp.write(QByteArrayLiteral("};\n\n"));
}

void ClassGenerator::writeDefinitions()
{
    const auto className = m_currentClass->name();

    writeFromJson();

    // ctor
    m_cpp.write(QStringView(u"%1::%1()\n").arg(className).toUtf8());
    indent(m_cpp);
    m_cpp.write(QStringView(u": d(new %1Private)\n{\n}\n\n").arg(className).toUtf8());

    // copy ctor
    m_cpp.write(QStringView(u"%1::%1(const %1 &) = default;\n\n").arg(className).toUtf8());

    // dtor
    m_cpp.write(QStringView(u"%1::~%1() = default;\n\n").arg(className).toUtf8());

    // assignment operator
    m_cpp.write(
        QStringView(u"%1 &%1::operator=(const %1 &) = default;\n\n").arg(className).toUtf8());

    const auto attributes = m_currentClass->attributes();
    for (const auto &attribute : attributes) {
        writeGetterSetterDefinition(attribute);
    }
}

void ClassGenerator::writeFromJson()
{
    using namespace JsonSchema;

    m_cpp.write(QStringView(u"std::optional<%1> %1::fromJson(const QJsonObject &json)\n{\n")
                    .arg(m_currentClass->name())
                    .toUtf8());

    indent(m_cpp);
    m_cpp.write("if (json.isEmpty())\n");
    indent(m_cpp, 2);
    m_cpp.write("return std::nullopt;\n\n");

    indent(m_cpp);
    m_cpp.write(QStringView(u"%1 object;\n").arg(m_currentClass->name()).toUtf8());

    const auto attributes = m_currentClass->attributes();
    for (const auto &attribute : attributes) {
        indent(m_cpp);
        const auto attributeAssignment = QStringView(u"object.d->%1 = ")
                                             .arg(m_jsonSchema.formatKeyName(attribute.jsonKey()));
        const auto jsonValueGetter = QStringView(u"json.value(u\"%1\")").arg(attribute.jsonKey());

        switch (attribute.type()) {
        case Attribute::Object:
            m_cpp.write(QStringView(uR"(if (auto attribute = %1::fromJson(%2.toObject()))
        %1 *attribute;
    else
        return std::nullopt;
)")
                            .arg(attribute.typeReference(), jsonValueGetter)
                            .toUtf8());
            break;
        case Attribute::Array:
            writeFromJsonArray(attribute);
            break;
        case Attribute::String:
            m_cpp.write(QStringView(u"%1%2.toString();\n")
                            .arg(attributeAssignment, jsonValueGetter)
                            .toUtf8());
            break;
        case Attribute::Bool:
            m_cpp.write(
                QStringView(u"%1%2.toBool();\n").arg(attributeAssignment, jsonValueGetter).toUtf8());
            break;
        case Attribute::Int8:
        case Attribute::Int16:
        case Attribute::Int32:
        case Attribute::Int64:
        case Attribute::UInt8:
        case Attribute::UInt16:
        case Attribute::UInt32:
        case Attribute::UInt64:
            m_cpp.write(attributeAssignment.toUtf8());
            m_cpp.write(attribute.cppType().toUtf8());
            m_cpp.write("(");
            m_cpp.write(jsonValueGetter.toUtf8());
            m_cpp.write(".toDouble());\n");
            break;
        default:
            m_cpp.write("/* FIXME */;\n");
        }
    }

    indent(m_cpp);
    m_cpp.write("return object;\n");

    m_cpp.write("}\n\n");
}

void ClassGenerator::writeFromJsonArray(const JsonSchema::Attribute &attribute)
{
    using namespace JsonSchema;

    const auto cppAttributeName = m_jsonSchema.formatKeyName(attribute.jsonKey());
    const auto jsonValueGetter = QStringView(u"json.value(u\"%1\")").arg(attribute.jsonKey());

    if (const auto arrayType = Attribute::jsonTypeFromString(attribute.typeReference())) {
        // TODO: handle basic types

        m_cpp.write(QStringView(uR"(const auto %1Array = %2.toArray();
    object.d->%1.reserve(%1Array.size());
    for (const auto &arrayItem : %1Array) {
        object.d->%1 << )")
                        .arg(cppAttributeName, jsonValueGetter)
                        .toUtf8());

        switch (*arrayType) {
        case Attribute::String:
            m_cpp.write(QStringView(u"arrayItem.toString();\n").toUtf8());
            break;
        case Attribute::Bool:
            m_cpp.write(QStringView(u"arrayItem.toBool();\n").toUtf8());
            break;
        case Attribute::Int8:
        case Attribute::Int16:
        case Attribute::Int32:
        case Attribute::Int64:
        case Attribute::UInt8:
        case Attribute::UInt16:
        case Attribute::UInt32:
        case Attribute::UInt64:
            m_cpp.write(Attribute::jsonTypeToString(*arrayType).toUtf8());
            m_cpp.write("(arrayItem.toDouble());\n");
            break;
        default:
            m_cpp.write("/* FIXME */;\n");
        }

        // close for {
        m_cpp.write("    }\n\n");

    } else {
        m_cpp.write(QStringView(uR"(const auto %1Array = %2.toArray();
    for (const auto &arrayItem : %1Array) {
        if (auto item = %3::fromJson(arrayItem)) {
            object.d->%1 << *item;
        }
    }
)")
                        .arg(m_jsonSchema.formatKeyName(attribute.jsonKey()),
                             jsonValueGetter,
                             attribute.typeReference())
                        .toUtf8());
    }
}

void ClassGenerator::writeGetterSetterDefinition(const JsonSchema::Attribute &attribute)
{
    // getter
    // <indent>Key key() const {
    const auto getter = QStringView(u"%1 %2::%3() const\n{\n")
                            .arg(attribute.cppType(),
                                 m_currentClass->name(),
                                 m_jsonSchema.getterName(attribute));

    m_cpp.write(getter.toUtf8());
    indent(m_cpp);
    m_cpp.write("return d->");
    m_cpp.write(m_jsonSchema.formatKeyName(attribute.jsonKey()).toUtf8());
    m_cpp.write(";\n}\n\n");

    // setter
    // <indent>void setKey(const Key &) {
    const auto setter = QStringView(u"void %1::%2(%3 value)\n{\n")
                            .arg(m_currentClass->name(),
                                 m_jsonSchema.setterName(attribute),
                                 attribute.cppSetterType());
    m_cpp.write(setter.toUtf8());

    indent(m_cpp);
    m_cpp.write("d->");
    m_cpp.write(m_jsonSchema.formatKeyName(attribute.jsonKey()).toUtf8());
    m_cpp.write(" = value;\n}\n\n");
}

void ClassGenerator::writeAttribute(const JsonSchema::Attribute &attribute)
{
    // <indent>Type key;
    indent(m_cpp, 1);
    m_cpp.write(attribute.cppType().toUtf8());
    m_cpp.write(QByteArrayLiteral(" "));
    m_cpp.write(m_jsonSchema.formatKeyName(attribute.jsonKey()).toUtf8());
    m_cpp.write(QByteArrayLiteral(" = {};\n"));
}

void ClassGenerator::writeInclude(QFile &file, QStringView filename, bool global)
{
    if (global) {
        file.write(QStringView(u"#include <%1>\n").arg(filename).toUtf8());
    } else {
        file.write(QStringView(u"#include \"%1\"\n").arg(filename).toUtf8());
    }
}

void ClassGenerator::forwardDeclare(QFile &file, QStringView className)
{
    file.write(QStringView(u"class %1;\n").arg(className).toUtf8());
}

void ClassGenerator::indent(QFile &file, unsigned int indentLevel)
{
    file.write(QByteArrayLiteral("    ").repeated(indentLevel));
}
