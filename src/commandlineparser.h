// SPDX-FileCopyrightText: 2020 Jonah Brüchert <jbb@kaidan.im>
// SPDX-FileCopyrightText: 2020 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#pragma once

#include <QCommandLineParser>
#include <QDir>

class CommandLineParser
{
public:
    CommandLineParser();

    void processArgs();

    QString inputFile() const;
    QDir outputDir() const;

private:
    QCommandLineParser m_parser;
    QString m_inputFile;
    QDir m_outputDir;
};
