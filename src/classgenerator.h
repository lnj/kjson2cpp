// SPDX-FileCopyrightText: 2020 Linus Jahn <lnj@kaidan.im>
// SPDX-FileCopyrightText: 2020 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#ifndef CLASSGENERATOR_H
#define CLASSGENERATOR_H

#include "commandlineparser.h"
#include "jsonschema.h"

class ClassGenerator
{
public:
    ClassGenerator(CommandLineParser &&parser);

    void generateCode();

private:
    void generateFilesForClass();

    void createHeaderFile();
    void createCppFile();

    void writeClassHeader();
    void writeGetterSetterHeader(const JsonSchema::Attribute &attribute);

    void writePrivateClass();
    void writeDefinitions();
    void writeFromJson();
    void writeFromJsonArray(const JsonSchema::Attribute &attribute);
    void writeGetterSetterDefinition(const JsonSchema::Attribute &attribute);
    void writeAttribute(const JsonSchema::Attribute &attribute);

    void writeInclude(QFile &file, QStringView filename, bool global = true);
    void forwardDeclare(QFile &file, QStringView className);
    void indent(QFile &file, unsigned int indentLevel = 1);

    CommandLineParser &&m_parser;
    JsonSchema::Schema m_jsonSchema;
    const JsonSchema::Class *m_currentClass;

    QFile m_header;
    QFile m_cpp;
};

#endif // CLASSGENERATOR_H
