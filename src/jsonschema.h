// SPDX-FileCopyrightText: 2020 Linus Jahn <lnj@kaidan.im>
// SPDX-FileCopyrightText: 2020 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#pragma once

#include <optional>
#include <QList>

class QDomElement;

namespace JsonSchema {

class Attribute
{
public:
    enum JsonType : quint16 {
        Object,
        Array,
        String,
        Bool,
        Int8,
        Int16,
        Int32,
        Int64,
        UInt8,
        UInt16,
        UInt32,
        UInt64,
    };
    static std::optional<JsonType> jsonTypeFromString(QStringView);
    static QStringView jsonTypeToString(JsonType);

    static std::optional<Attribute> fromDom(const QDomElement &);

    Attribute();

    JsonType type() const;
    QString jsonKey() const;
    QString typeReference() const;

    QString cppType() const;
    QString cppSetterType() const;

private:
    JsonType m_type;
    QString m_jsonKey;
    QString m_typeReference;
};

class Class
{
public:
    static std::optional<Class> fromDom(const QDomElement &);

    QString name() const;
    void setName(const QString &name);

    QList<Attribute> attributes() const;
    void setAttributes(const QList<Attribute> &attributes);

private:
    QString m_name;
    QList<Attribute> m_attributes;
};

class Schema
{
public:
    enum KeyCasing { CamelCase, SnakeCase, KebapCase };
    static std::optional<KeyCasing> keyCasingFromString(const QString &);

    enum GetterNameStyle {
        IncludeGet,
        WithoutGet,
    };
    static std::optional<GetterNameStyle> getterNameStyleFromString(const QString &);

    static std::optional<Schema> fromDom(const QDomElement &);

    Schema();

    std::optional<KeyCasing> keyCasing() const;
    QList<Class> classes() const;

    QString getterName(const Attribute &attribute);
    QString setterName(const Attribute &attribute);
    QString formatKeyName(QStringView name);

    QSet<QString> referencedClasses(const QString &className) const;

private:
    QString prependToFunctionName(QStringView string, QStringView prepend);

    QList<Class> m_classes;
    std::optional<KeyCasing> m_keyCasing;
    GetterNameStyle m_getterNameStyle;
};

} // namespace JsonSchema
