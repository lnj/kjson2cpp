// SPDX-FileCopyrightText: 2020 Jonah Brüchert <jbb@kaidan.im>
// SPDX-FileCopyrightText: 2020 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#include <QCoreApplication>

#include "classgenerator.h"
#include "commandlineparser.h"

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    CommandLineParser parser;
    parser.processArgs();

    ClassGenerator generator(std::move(parser));
    generator.generateCode();
    return 0;
}
