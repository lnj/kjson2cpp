﻿// SPDX-FileCopyrightText: 2020 Jonah Brüchert <jbb@kaidan.im>
// SPDX-FileCopyrightText: 2020 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#include "commandlineparser.h"

CommandLineParser::CommandLineParser() {}

void CommandLineParser::processArgs()
{
    m_parser.setApplicationDescription(QStringLiteral("Qt JSON parser class generator"));
    m_parser.addHelpOption();
    m_parser.addVersionOption();

    const QCommandLineOption inputOption = {{QStringLiteral("i"), QStringLiteral("input")},
                                            QStringLiteral("Input file name (required)"),
                                            QStringLiteral("input")};
    const QCommandLineOption outputOption = {{QStringLiteral("o"), QStringLiteral("output")},
                                             QStringLiteral("Output directory (required)"),
                                             QStringLiteral("output")};

    m_parser.addOption(inputOption);
    m_parser.addOption(outputOption);

    m_parser.process(*QCoreApplication::instance());
    if (!m_parser.isSet(inputOption) || !m_parser.isSet(outputOption)) {
        m_parser.showHelp();
    }

    m_inputFile = m_parser.value(inputOption);
    m_outputDir = m_parser.value(outputOption);
}

QString CommandLineParser::inputFile() const
{
    return m_inputFile;
}

QDir CommandLineParser::outputDir() const
{
    return m_outputDir;
}
