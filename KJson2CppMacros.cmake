# SPDX-FileCopyrightText: 2020 Linus Jahn <lnj@kaidan.im>
# SPDX-FileCopyrightText: 2020 Jonah Brüchert <jbb@kaidan.im>
#
# SPDX-License-Identifier: CC0-1.0

function(kjson2cpp_add_schema target schema_file)
    # the command for creating the source file from the kcfg file
    add_custom_command(
        OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/kjson2cpp/target.cpp
        COMMAND KJson2Cpp::kjson2cpp
        ARGS --input ${CMAKE_CURRENT_SOURCE_DIR}/${schema_file} --output ${CMAKE_CURRENT_BINARY_DIR}/kjson2cpp
        MAIN_DEPENDENCY ${CMAKE_CURRENT_SOURCE_DIR}/${schema_file}
        DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/${schema_file} KJson2Cpp::kjson2cpp
    )

    target_sources(${target} PRIVATE ${CMAKE_CURRENT_BINARY_DIR}/kjson2cpp/target.cpp)
    target_include_directories(${target} PRIVATE ${CMAKE_CURRENT_BINARY_DIR}/kjson2cpp)
endfunction()
