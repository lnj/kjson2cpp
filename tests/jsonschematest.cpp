// SPDX-FileCopyrightText: 2020 Jonah Brüchert <jbb@kaidan.im>
// SPDX-FileCopyrightText: 2020 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#include <QDomDocument>
#include <QTest>

#include <verdigris/wobjectimpl.h>

#include "jsonschema.h"

class JsonSchemaTest : public QObject
{
    W_OBJECT(JsonSchemaTest);

private:
    std::optional<QDomElement> parseJson(const QByteArray &xml)
    {
        QDomDocument document;
        if (!document.setContent(xml))
            return std::nullopt;
        return document.documentElement();
    }

    std::optional<JsonSchema::Schema> parseSchema(const QByteArray &xml)
    {
        return JsonSchema::Schema::fromDom(parseJson(xml).value_or(QDomElement()));
    }

    void testJsonSchema()
    {
        auto schema = parseSchema(QByteArrayLiteral(
            R"(<?xml version="1.0" encoding="UTF-8"?>)"
            R"(<json-schema>)"
            R"(<key-conversion type="camelCase"/>)"
            R"(<class name="Blah" type="custom" cpp-type="QJsonObject"/>)"
            R"(<class name="MyJson">)"
            R"(<attribute json-key="key" type="string" />)"
            R"(<attribute json-key="key" type="int32" />)"
            R"(<attribute json-key="key" type="uint32" />)"
            R"(<attribute json-key="key" type="uint8" />)"
            R"(<attribute json-key="blah" type="object" reference="Blah" />)"
            R"(<attribute json-key="object_list" type="array" reference="Blah" />)"
            R"(</class>)"
            R"(</json-schema>)"));

        QVERIFY(schema->keyCasing().has_value());
        QCOMPARE(*schema->keyCasing(), JsonSchema::Schema::CamelCase);
        QCOMPARE(schema->classes().at(0).name(), QStringLiteral("Blah"));
        QCOMPARE(schema->classes().at(1).name(), QStringLiteral("MyJson"));

        QCOMPARE(schema->classes().at(0).attributes().count(), 0);
        const auto attributes = schema->classes().at(1).attributes();

        QCOMPARE(attributes.at(0).type(), JsonSchema::Attribute::String);
        QCOMPARE(attributes.at(1).type(), JsonSchema::Attribute::Int32);
        QCOMPARE(attributes.at(2).type(), JsonSchema::Attribute::UInt32);
        QCOMPARE(attributes.at(3).type(), JsonSchema::Attribute::UInt8);
        QCOMPARE(attributes.at(3).typeReference().isNull(), true);
        QCOMPARE(attributes.at(4).type(), JsonSchema::Attribute::Object);
        QCOMPARE(attributes.at(4).typeReference(), "Blah");
    }
    W_SLOT(testJsonSchema, W_Access::Private);

    JsonSchema::Schema m_schema;
};

W_OBJECT_IMPL(JsonSchemaTest)

QTEST_GUILESS_MAIN(JsonSchemaTest);
