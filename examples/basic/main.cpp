#include <optional>

#include "VideoInfo.h"

#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>

int main()
{
    const QByteArray jsonData(R"({
        "title": "Krazy Kats",
        "lengthSeconds": 202,
        "liveNow": true
    })");

    const auto videoInfo = VideoInfo::fromJson(QJsonDocument::fromJson(jsonData).object());

    qDebug() << "Title:\t\t\t" << videoInfo->title();
    qDebug() << "Length (seconds):\t" << videoInfo->lengthSeconds();
    qDebug() << "Live now:\t\t" << videoInfo->liveNow();

    return 0;
}
